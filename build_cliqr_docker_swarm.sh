#!/bin/bash
cd ..
rm -rf cliqr_docker_swarm_manager*
rm -rf cliqr_docker_swarm_worker*
mkdir cliqr_docker_swarm_manager cliqr_docker_swarm_worker
cp cliqr_docker_swarm/manager cliqr_docker_swarm_manager/.
cp cliqr_docker_swarm/worker cliqr_docker_swarm_worker/.
zip -r cliqr_docker_swarm_manager.zip cliqr_docker_swarm_manager -x *.git*
zip -r cliqr_docker_swarm_worker.zip cliqr_docker_swarm_worker -x *.git*
